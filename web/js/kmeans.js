if (!Detector.webgl)
	Detector.addGetWebGLMessage();

var container, stats, controls;
var camera, scene, renderer, particles, geometry, material, i, h, color, colors = [], sprite, size;
var centparticles, centgeometry, centmatrial, centcolors = [];

var keyboard = new THREEx.KeyboardState();

var firstIter = true;
var numCentroids;
var centIndex = 0;

var centroids = [];

var points = [];

var seeds;
var uniforms;
var shaderMaterial;

var values_color;
var attributes;

init();
animate();

function init() {

	camera = new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 1, 10000);
	camera.position.z = 1000;

	scene = new THREE.Scene();
	scene.fog = new THREE.FogExp2(0x000000, 0.0009);

	attributes = {

		size : {
			type : 'f',
			value : null
		},
		customColor : {
			type : 'c',
			value : null
		}

	};

	uniforms = {

		amplitude : {
			type : "f",
			value : 1.0
		},
		color : {
			type : "c",
			value : new THREE.Color(0xffffff)
		},
		texture : {
			type : "t",
			value : THREE.ImageUtils.loadTexture("img/spark1.png")
		},

	};

	shaderMaterial = new THREE.ShaderMaterial({

		uniforms : uniforms,
		attributes : attributes,
		vertexShader : document.getElementById('vertexshader').textContent,
		fragmentShader : document.getElementById('fragmentshader').textContent,

		blending : THREE.AdditiveBlending,
		depthTest : false,
		transparent : true

	});

	sprite = THREE.ImageUtils.loadTexture("img/spark1.png");

	renderer = new THREE.WebGLRenderer();
	renderer.setSize(window.innerWidth, window.innerHeight);

	container = document.createElement('div');
	document.body.appendChild(container);
	container.appendChild(renderer.domElement);

	controls = new THREE.TrackballControls(camera, renderer.domElement);

	stats = new Stats();
	stats.domElement.style.position = 'absolute';
	stats.domElement.style.top = '0px';
	container.appendChild(stats.domElement);

}

function drawCentroid() {

	//console.log("centroids.length: " + centroids.length);
	centgeometry = new THREE.Geometry();
	for ( i = 0; i < centroids.length; i++) {

		centcolors[i] = new THREE.Color(0xffffff);
		centcolors[i].setHSL(Math.random(), 1, 0.5);

		//console.log("#: " + i);
		//console.log("CENTCOLOR: " + centcolors[i]);
	}

	centgeometry.vertices = centroids;

	centgeometry.colors = centcolors;

	centmaterial = new THREE.ParticleBasicMaterial({
		size : 150,
		map : sprite,
		vertexColors : true,
		transparent : false
	});
	centmaterial.alphaTest = 0.5;

	centparticles = new THREE.ParticleSystem(centgeometry, centmaterial);
	centparticles.sortParticles = true;

	scene.add(centparticles);

}

function animate() {

	requestAnimationFrame(animate);

	//particles.rotation.y += 0.01;
	//centparticles.rotation.y += 0.01;
	render();
	stats.update();
	controls.update();
	if (keyboard.pressed("z")) {
		camera.position.set(0, 150, 400);
	}

}

function render() {

	renderer.render(scene, camera);

}

function importPoint(pt) {
	if (pt.length == 3) {
		for (var i = 0; i < pt.length; i++) {
			points.push(pt[i]);
		}
		/*
		 var vertex = new THREE.Vector3();
		 vertex.x = pt[0];
		 vertex.y = pt[1];
		 vertex.z = pt[2];
		 vertex.color = new THREE.Color(0xff0000);
		 points.push(vertex);
		 */
	} else {
		console.log("Need 3 points! Not: " + pt.length);

	}
}

function addPoints() {
	particles = points.length / 3;
	//console.log(points);
	//console.log("# of particles:" + particles);
	geometry = new THREE.BufferGeometry();
	geometry.dynamic = true;
	geometry.attributes = {

		position : {
			itemSize : 3,
			array : new Float32Array(particles * 3),
			numItems : particles * 3
		},
		customColor : {
			itemSize : 3,
			array : new Float32Array(particles * 3),
			numItems : particles * 3
		},
		size : {
			itemSize : 1,
			array : new Float32Array(particles),
			numItems : particles * 1,
			dynamic : true
		},

	}

	values_size = geometry.attributes.size.array;
	var positions = geometry.attributes.position.array;
	//positions = points;
	values_color = geometry.attributes.customColor.array;

	seeds = new THREE.ParticleSystem(geometry, shaderMaterial);

	//sphere.sortParticles = true;

	var randColor = new THREE.Color(0xffaa00);

	for (var v = 0; v < particles; v++) {

		values_size[v] = 5;

		positions[v * 3 + 0] = points[v * 3 + 0];
		positions[v * 3 + 1] = points[v * 3 + 1];
		positions[v * 3 + 2] = points[v * 3 + 2];

		randColor.setHSL(Math.random(), Math.random(), 0.5);

		values_color[v * 3 + 0] = randColor.r;
		values_color[v * 3 + 1] = randColor.g;
		values_color[v * 3 + 2] = randColor.b;

	}

	//console.log(positions);

	/*
	 geometry = new THREE.Geometry();
	 for ( i = 0; i < points.length; i++) {
	 colors[i] = new THREE.Color(0xffffff);
	 colors[i].setHSL(Math.random(), 1, 0.5);
	 }
	 */

	/*
	 geometry.vertices = points;
	 geometry.colors = colors;

	 material = new THREE.ParticleBasicMaterial({
	 size : 25,
	 map : sprite,
	 vertexColors : true,
	 transparent : true
	 });
	 material.alphaTest = 0.5;

	 particles = new THREE.ParticleSystem(geometry, material);
	 particles.sortParticles = true;
	 */

	scene.add(seeds);

}

function updateCentroid(cent) {
	if (cent.length == 3) {
		//console.log("centIndex: " + centIndex);

		var vertex = new THREE.Vector3();
		//console.log(cent[0] + "," + cent[1] + "," + cent[2])
		vertex.x = cent[0];
		vertex.y = cent[1];
		vertex.z = cent[2];
		vertex.color = new THREE.Color(0xff0000);
		centroids[centIndex] = vertex;
		/*
		if (!firstIter) {
			updateColor();
			//console.log("coloring!");
		}
		*/
		if (centIndex == numCentroids) {
			if (firstIter) {
				//console.log("drawCentroid");
				drawCentroid();
				firstIter = false;
			}
			else {
				updateColor();

			}
			centIndex = 0;

		} else {
			centIndex++;
		}

	} else {
		console.log("Need 3 points! Not: " + cent.length);

	}

}

function setNumCentroids(k) {
	numCentroids = k - 1;
	//console.log("numCentroids: " + numCentroids);

}

function updateColor() {
	var nearest;
	//var newColor = new THREE.Color(0xff0000);
	for ( v = 0; v < particles; v++) {
		nearest = findNearestCentroid(points[v * 3 + 0], points[v * 3 + 1], points[v * 3 + 2]);

		values_color[ v * 3 + 0 ] = centcolors[nearest].r;
		values_color[ v * 3 + 1 ] = centcolors[nearest].g;
		values_color[ v * 3 + 2 ] = centcolors[nearest].b;
	}


	geometry.attributes.customColor.needsUpdate = true;
	//geometry.colorsNeedUpdate = true;

}

function findNearestCentroid(t1, t2, t3) {
	var nearest, dist = Infinity
	for (var i = 0, len = centroids.length; i < len; i++) {
		var d = Math.pow(centroids[i].x - t1, 2) + Math.pow(centroids[i].y - t2, 2) + Math.pow(centroids[i].z - t3, 2)
		if (d < dist) {
			nearest = i
			dist = d
		}
	}
	return nearest
}