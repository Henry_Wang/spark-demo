package com.wandisco.demo;

//package socks;

import spark.examples.JavaKMeans;

import java.io.*;
import java.util.LinkedList;
import java.util.Scanner;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.Collection;

import org.java_websocket.WebSocket;
import org.java_websocket.WebSocketImpl;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

public class DataServer extends WebSocketServer {

	LinkedList<String> buffer = new LinkedList<String>();

	public DataServer(int port) throws UnknownHostException {
		super(new InetSocketAddress(port));
		System.out.println("New DataServer");
	}

	public DataServer(InetSocketAddress address) {
		super(address);
	}

	@Override
	public void onMessage(WebSocket conn, String message) {
		// this.sendToAll( message );
		//System.out.println(conn + ": " + message);
	}

	@Override
	public void onClose(WebSocket arg0, int arg1, String arg2, boolean arg3) {
		// TODO Auto-generated method stub
		//System.out.println("Server onClose: " + arg2);
	}

	@Override
	public void onError(WebSocket arg0, Exception arg1) {
		// TODO Auto-generated method stub
		// System.out.println("onError");
		arg1.printStackTrace();
		if (arg0 != null) {
			// System.out.println("websocket not null");
			// some errors like port binding failed may not be assignable to a
			// specific websocket
		}

	}

	@Override
	public void onOpen(WebSocket arg0, ClientHandshake arg1) {
		// TODO Auto-generated method stub
		//System.out.println("New Connection");
		sendToAll("");

	}

	/**
	 * Sends <var>text</var> to all currently connected WebSocket clients.
	 * 
	 * @param text
	 *            The String to send across the network.
	 * @throws InterruptedException
	 *             When socket related I/O errors occur.
	 */
	public void sendToAll(String text) {
		// System.out.println("sendToAll: ");
		Collection<WebSocket> con = connections();
		String temp;
		if (con.isEmpty()) {
			buffer.add(text);
		} else {
			synchronized (con) {
				while ((temp = buffer.poll()) != null) {
					for (WebSocket c : con) {
						c.send(temp);
					}
				}
				for (WebSocket c : con) {
					if (text != "") {c.send(text);}
					
					// System.out.println("Sent: " + text);
				}
			}

		}

	}

	public static void main(String[] args) throws InterruptedException,
			IOException {
		if (args.length < 5) {
			System.err
					.println("Usage: DataServer <port> <master> <file> <k> <convergeDist>");

			System.exit(1);
		}
		// WebSocketImpl.DEBUG = true;
		int port = 8887; // 843 flash policy port
		try {
			port = Integer.parseInt(args[0]);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		System.out.println("K:" + args[3]);
		final DataServer s = new DataServer(port);
		s.start();
		System.out.println("DataServer started on port: " + s.getPort());

		PipedOutputStream pipeOut = new PipedOutputStream();
		PipedInputStream pipeIn = new PipedInputStream(pipeOut);
		System.setOut(new PrintStream(pipeOut));

		String arguments[] = new String[4];
		for (int i = 0; i < 4; i++) {
			arguments[i] = args[i + 1];
		}

		//send K
		s.sendToAll("(" + (args[3]) + ")");

		class KMeansThread extends Thread {
			final String KArgs[];

			KMeansThread(String[] pass) {
				KArgs = pass;
			}

			@Override
			public void run() {
				try {
					//System.out.println("starting K MEANS");
					JavaKMeans.main(KArgs);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}

		Thread KMeansThread = new KMeansThread(arguments);
		KMeansThread.start();

		Scanner scanIn = new Scanner(pipeIn);
		try {

			while (scanIn.hasNextLine()) {
				s.sendToAll(scanIn.nextLine());

			}
		} finally {
			if (scanIn != null) {
				scanIn.close();
			}
		}

		// System.exit(0);
	}

}
